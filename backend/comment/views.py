from django.shortcuts import render
from .models import Comment
from .serializers import CommentSerializer
from rest_framework.generics import (ListCreateAPIView, RetrieveUpdateDestroyAPIView)

# this class will view and insert the comments
class CommentListCreateView(ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

# this class will delete,update and retrive the comments
class CommentDetailUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    lookup_field = 'pk'
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

